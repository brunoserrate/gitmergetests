SELECT *
FROM customers;

-- #1
SELECT customers.name,
    customers.email,
    COUNT(orders.order_id) as order_count
FROM customers
    LEFT JOIN orders ON customers.customer_id = orders.customer_id
GROUP BY customers.customer_id;